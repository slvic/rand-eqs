package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"gitlab.com/slvic/rand-eqs/internal/eqsgen"
)

func main() {
	godotenv.Load()

	port := os.Getenv("SRV_PORT")

	mux := http.NewServeMux()
	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		firstColumnEqs, secondColumnEqs, err := eqsgen.Get24Eqs()
		if err != nil {
			w.Write([]byte(err.Error()))
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		eqs := "le eqs \n"

		for i := 0; i < eqsgen.OneColumnVolume; i++ {
			eqs = fmt.Sprintf("%s %-12s | %6s \n", eqs, firstColumnEqs[i], secondColumnEqs[i])
		}

		w.Write([]byte(eqs))
	})

	srv := http.Server{
		Addr:    port,
		Handler: mux,
	}

	log.Println("Server is starting on port " + port)
	srv.ListenAndServe()
}
