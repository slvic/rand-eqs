package eqsgen

import (
	"errors"
	"fmt"
)

func generateFirstColumn(firstMembers, secondMembers []int64) ([]string, error) {
	if len(firstMembers) != len(secondMembers) {
		return nil, errors.New("quantities of the first and the second members have to be equal")
	}

	equations := make([]string, 0, len(firstMembers))
	multiplyTemplate := `%d * %d = `
	divideTemplate := `%d / %d = `

	for i := 0; i < len(firstMembers); i = i + 2 {
		equations = append(equations, fmt.Sprintf(multiplyTemplate, firstMembers[i], secondMembers[i]))
		equations = append(equations, fmt.Sprintf(divideTemplate, firstMembers[i+1], secondMembers[i+1]))
	}

	return equations, nil
}

func generateSecondColumn(firstMembers, secondMembers []int64) ([]string, error) {
	if len(firstMembers) != len(secondMembers) {
		return nil, errors.New("quantities of the first and the second members have to be equal")
	}

	equations := make([]string, 0, len(firstMembers))
	addupTemplate := `%d + %d = `
	takeawayTemplate := `%d - %d = `

	for i := 0; i < len(firstMembers); i = i + 4 {
		equations = append(equations, fmt.Sprintf(addupTemplate, firstMembers[i], secondMembers[i]))
		equations = append(equations, fmt.Sprintf(addupTemplate, firstMembers[i+1], secondMembers[i+1]))

		equations = append(equations, fmt.Sprintf(takeawayTemplate, firstMembers[i+2], secondMembers[i+2]))
		equations = append(equations, fmt.Sprintf(takeawayTemplate, firstMembers[i+3], secondMembers[i+3]))
	}

	return equations, nil
}
