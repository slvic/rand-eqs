package eqsgen

import (
	"os"

	"github.com/sgade/randomorg"
)

const OneColumnVolume = 12

func Get24Eqs() (firstColumnEqs, secondColumnEqs []string, err error) {
	randorg := randomorg.NewRandom(os.Getenv("RANDORG_API_KEY"))

	firstColumnFirstMembers, err := randorg.GenerateIntegers(OneColumnVolume, 1, 144)
	if err != nil {
		return nil, nil, err
	}
	firstColumnSecondMembers, err := randorg.GenerateIntegers(OneColumnVolume, 1, 12)
	if err != nil {
		return nil, nil, err
	}

	firstColumnEqs, err = generateFirstColumn(firstColumnFirstMembers, firstColumnSecondMembers)
	if err != nil {
		return nil, nil, err
	}

	secondColumnFirstMembers, err := randorg.GenerateIntegers(OneColumnVolume, 1, 144)
	if err != nil {
		return nil, nil, err
	}
	secondColumnSecondMembers, err := randorg.GenerateIntegers(OneColumnVolume, 1, 144)
	if err != nil {
		return nil, nil, err
	}

	secondColumnEqs, err = generateSecondColumn(secondColumnFirstMembers, secondColumnSecondMembers)
	if err != nil {
		return nil, nil, err
	}

	return
}
