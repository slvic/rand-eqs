module gitlab.com/slvic/rand-eqs

go 1.20

require (
	github.com/joho/godotenv v1.5.1
	github.com/sgade/randomorg v0.0.0-20190406144034-640a49fc64e0
)

require (
	github.com/google/uuid v1.0.0 // indirect
	github.com/pborman/uuid v1.2.1 // indirect
)
